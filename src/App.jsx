import { useState } from "react";

import { CounterTitle } from "./components/CounterTitle";
import { Refresh } from "./components/Refresh";
import { Item } from "./components/Item";

const data =
{
  items_selected: 0,
  items: [
    {
      id: 1,
      count: "Zero",

    },
    {
      id: 2,
      count: "Zero",
    },
    {
      id: 3,
      count: "Zero",
    },
    {
      id: 4,
      count: "Zero",
    }
  ],
}



function App() {
  const [givenData, setGivenData] = useState(JSON.parse(JSON.stringify(data)))

  return (
    <div className="main-container">
      <CounterTitle count={givenData.items_selected} />
      {console.log(data)}
      <Refresh modifiedData={givenData} setItems={setGivenData} givenData={data} />
      {givenData.items.map(item => {
        return <Item key={item.id} data={givenData} setData={setGivenData} count={item.count} id={item.id} />
      })}
    </div>
  );
}

export default App;
