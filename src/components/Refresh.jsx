import React from "react";

export const Refresh = ({ modifiedData, setItems, givenData }) => {
  const recycleData = () => {
    if (modifiedData.items.length == 0) {
      setItems(JSON.parse(JSON.stringify(givenData)));
    }
  };
  const refreshData = () => {
    modifiedData.items = modifiedData.items.map((item) => {
      item.count = "Zero";
      return item;
    });
    modifiedData.items_selected = 0;
    console.log("nxbcvhvhf", modifiedData);
    setItems(JSON.parse(JSON.stringify(modifiedData)));
  };
  return (
    <div className="refresh-component">
      <button className="refresh" onClick={refreshData}>
        <i className="fa-solid fa-rotate" style={{ color: "#f7f7f8" }}></i>
      </button>

      <button
        className={
          (modifiedData.items.length == 0 && "refresh recycle-active") ||
          "recycle"
        }
        onClick={recycleData}
      >
        <i className="fa-solid fa-recycle"></i>
      </button>
    </div>
  );
};
