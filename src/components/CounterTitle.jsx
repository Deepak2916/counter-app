import React from "react";

export const CounterTitle = ({count}) => {
  return <div className="CounterTitle">
    <i className="fa-solid fa-cart-shopping fa-2xl"></i>
    <div><span>{count}</span></div>
    <span>items</span>
  </div>;
};
