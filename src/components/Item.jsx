import React from "react";

export const Item = ({ data, setData, count, id }) => {
  const incrementCount = () => {
    const newData = { ...data };

    newData.items = newData.items.map((item) => {
      if (item.id == id) {
        newData.items_selected =
          (item.count == "Zero" && newData.items_selected + 1) ||
          newData.items_selected;
        item.count = (item.count == "Zero" && 1) || item.count + 1;
      }
      return item;
    });
    setData(newData);
  };

  const decrementCount = () => {
    const newData = { ...data };

    newData.items = newData.items.map((item) => {
      if (item.id == id) {
        console.log(item.count == 1 && newData.items_selected - 1);

        if (item.count == 1) {
          newData.items_selected = newData.items_selected - 1;
        }
        item.count =
          (item.count !== "Zero" && item.count > 1 && item.count - 1) || "Zero";
      }
      return item;
    });
    setData(newData);
  };

  const deleteItem = () => {
    const newData = { ...data };
    newData.items_selected =
      (newData.items_selected > 0 && newData.items_selected - 1) || 0;
    newData.items = newData.items.filter((item) => {
      return item.id !== id;
    });
    setData(newData);
  };

  return (
    <div className="item-component">
      <div
        className={
          (count == "Zero" && "item-count") || "item-count count-active"
        }
      >
        <span>{count}</span>
      </div>

      <button className="increment border" onClick={incrementCount}>
        <i className="fa-solid fa-circle-plus fa-xl"></i>
      </button>

      <button
        onClick={decrementCount}
        className={
          (count != "Zero" && "decrement decrement-active") || "decrement"
        }
     
      >
        <i className="fa-solid fa-circle-minus fa-xl"></i>
      </button>

      <button onClick={deleteItem} className="delete">
        <i
          className="fa-regular fa-trash-can fa-xl"
          style={{ color: "#ededed" }}
        ></i>
      </button>
    </div>
  );
};
